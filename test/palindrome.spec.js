import { Palindrome } from '../src/palindrome'

describe(Palindrome, ()=>{

  it('check if it is a palindrom', ()=>{
    const a = 'Logra Casillas allí sacar gol'

    const result = new Palindrome()

    expect(result.isPalindrome(a)).toEqual('Es palíndromo')
  })
})