export class Nif{
  validateNIF(nif){
    let number = 0;
    let letter = '';
    let checkedLetter = '';
    const regEx = /^[0-9]{8}[a-zA-Z]{1}$/
    
    if(regEx.test(nif) === true){
      number = nif.substr(0, nif.length-1);
      letter = nif.substr(nif.length-1, 1);
      number = number % 23;
      checkedLetter = 'TRWAGMYFPDXBNJZSQVHLCKET'
      checkedLetter = checkedLetter.substring(number, number+1);
     
       if(checkedLetter!=letter.toUpperCase()){
       return 'Error: invalid letter'
       }else{
        return 'Valid NIF'
       }
    } else {
     return 'Error: not valid format'
    }
   }

}

 