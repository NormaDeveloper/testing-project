export class Palindrome{
   isPalindrome(str) {
    const tunedString = str.normalize("NFD").replace(/[\u0300-\u036f\W_]/g, "").toLowerCase();
   
    const strReversed = tunedString.split("").reverse().join("");
  
    return tunedString === strReversed ? "Es palíndromo" : "No es palíndromo";
  }
}